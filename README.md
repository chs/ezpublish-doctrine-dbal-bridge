composer.json

    {
        "require" : {
            "chs/ezpublish-doctrine-dbal-bridge": "~1.0"
        }
    }

site.ini

    [ExtensionSettings]
    ActiveExtensions[]=chs-doctrine-dbal-bridge

    [DatabaseSettings]
    DatabaseImplementation=doctrine_dbal

    [DoctrineBridge]
    Driver=pdo_mysql

.gitignore

    extension/chs-doctrine-dbal-bridge
