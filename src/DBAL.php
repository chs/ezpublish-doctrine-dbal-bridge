<?php
namespace Chs\Ezpublish\Bridge\Doctrine;

use Doctrine\DBAL\Driver\ServerInfoAwareConnection;
use eZDBInterface;
use eZINI;
use eZDebug;
use PDO;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\ConnectionException;

class DBAL extends eZDBInterface {
    public function __construct(array $parameters) {
        parent::__construct($parameters);

        $connectionParams = [
            'dbname' => $this  ->  DB,
            'user' => $this -> User,
            'password' => $this -> Password,
            'host' => $this -> Server,
            /**
             * force dash removal
             * because of kinda flawed \eZCharsetInfo::realCharsetCode()
             * using \eZCharsetInfo::aliasTable
             */
            'charset' => str_replace('-', '', $this -> Charset),
            'driver' => eZINI::instance()
                -> variable('DoctrineBridge', 'Driver'),
        ];

        if ('pdo_sqlite' === $connectionParams['driver']) {
            $connectionParams['path'] =
                eZINI::instance() -> variable('FileSettings', 'VarDir')
                . DIRECTORY_SEPARATOR
                . $connectionParams['dbname'];
        }

        $this -> DBConnection = DriverManager::getConnection($connectionParams);
        $this -> DBConnection -> connect();
        $this -> IsConnected = $this -> DBConnection -> isConnected();
    }

    public function arrayQuery($sql, $params = [], $server = false) {
        $retArray = [];

        if ($this -> IsConnected) {
            $limit = null;
            $offset = null;

            // check for array parameters
            if (is_array($params)) {
                if (array_key_exists('limit', $params) && is_numeric($params['limit'])) {
                    $limit = $params['limit'];
                }

                if (array_key_exists('offset', $params) && is_numeric($params['offset'])) {
                    $offset = $params['offset'];
                }
            }

            $result = $this -> query(
                $this -> DBConnection -> getDatabasePlatform()
                    -> modifyLimitQuery($sql, $limit, $offset),
                $server
            );

            if ($result === false) {
                $this -> reportQuery(__CLASS__, $sql, false, false);

                return false;
            }

            $retArray = $result -> fetchAll(PDO::FETCH_ASSOC);
        }

        return $retArray;
    }
    public function availableDatabases() {
        return $this -> DBConnection -> getSchemaManager() -> listDatabases();
    }

    public function beginQuery() {
        $this -> DBConnection -> beginTransaction();

        return true;
    }

    /** looks pretty useless and without any real use case */
    public function bindingType() {
        return self::BINDING_NO;
    }

    public function bindVariable($value, $fieldDef = false) {
        return $value;
    }

    public function bitAnd($arg1, $arg2) {
        return $this -> DBConnection -> getDatabasePlatform()
            -> getBitAndComparisonExpression($arg1, $arg2);
    }

    public function bitOr($arg1, $arg2) {
        return $this -> DBConnection -> getDatabasePlatform()
            -> getBitOrComparisonExpression($arg1, $arg2);
    }

    public function close() {
        $this -> DBConnection -> close();
        $this -> IsConnected = false;
    }

    public function commitQuery() {
        $this -> DBConnection -> commit();

        return true;
    }

    public function concatString($strings = []) {
        return $this -> DBConnection -> getDatabasePlatform()
            -> getConcatExpression($strings);
    }

    public function createDatabase($dbName) {
        $this -> DBConnection -> getSchemaManager() -> createDatabase($dbName);
        $this -> setError();
    }

    public function databaseClientVersion() {
        $wrappedConnection = $this -> DBConnection -> getWrappedConnection();
        $version = false;

        // Support DBAL 2.4 by accessing it directly when using PDO PgSQL
        if ($wrappedConnection instanceof \PDO) {
            $version = $wrappedConnection -> getAttribute(\PDO::ATTR_CLIENT_VERSION);
        }

        if (false !== $version) {
            $version = [
                'string' => $version,
                'values' => explode('.', $version),
            ];
        }

        return $version;
    }

    public function databaseName() {
        return __CLASS__;
    }

    public function databaseServerVersion() {
        $wrappedConnection = $this -> DBConnection -> getWrappedConnection();
        $version = false;

        if ($wrappedConnection instanceof ServerInfoAwareConnection) {
            $version = $wrappedConnection -> getServerVersion();
        }

        // Support DBAL 2.4 by accessing it directly when using PDO PgSQL
        if ($wrappedConnection instanceof \PDO) {
            $version =  $wrappedConnection -> getAttribute(\PDO::ATTR_SERVER_VERSION);
        }

        if (false !== $version) {
            $version = [
                'string' => $version,
                'values' => explode('.', $version),
            ];
        }

        return $version;
    }

    public function dropTempTable($dropTableQuery = '', $server = self::SERVER_SLAVE) {
        /** @todo STUB */
        eZDebug::writeError(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), 'eZDBInterface method not implemented');
    }

    public function escapeString($str) {
        return trim(
            $this -> DBConnection -> quote($str),
            $this -> DBConnection -> getDatabasePlatform()
                -> getStringLiteralQuoteCharacter()
        );
    }

    public function eZTableList($server = eZDBInterface::SERVER_MASTER) {
        return array_fill_keys(
            array_filter(
                $this -> DBConnection -> getSchemaManager() -> listTableNames(),
                function ($tableName) {
                    return 0 === strpos($tableName, 'ez');
                }
            ),
            eZDBInterface::RELATION_TABLE
        );
    }

    public function isCharsetSupported($charset) {
        return true;
    }

    public function isConnected() {
        /* If it claims to be connected, check if it really is */
        if ($this -> IsConnected) {
            $this -> IsConnected = $this -> DBConnection -> ping();
        }

        return $this -> IsConnected;
    }

    public function lastSerialID($table = false, $column = false) {
        return $this -> DBConnection -> lastInsertId();
    }

    public function lock($table) {
        /** @todo STUB */
        eZDebug::writeError(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), 'eZDBInterface method not implemented');
    }

    public function md5($str) {
        /* Let PHP compute MD5 hashes
         * as the function is missing from some doctrine platforms (e.g. : sqlite)
         * and the use cases are all on PHP variables not SQL column names
         */
        return $this -> DBConnection -> getDatabasePlatform() -> quoteStringLiteral(md5($str));
    }

    public function query($sql, $server = false) {
        return $this -> isConnected() ? $this -> DBConnection -> query($sql) : false;
    }

    public function relationCount($relationType = eZDBInterface::RELATION_TABLE) {
        return count($this -> relationList($relationType));
    }

    public function relationCounts($relationMask) {
        $count = 0;

        if ($relationMask & self::RELATION_TABLE) {
            $count += $this -> relationCount(self::RELATION_TABLE);
        }

        if ($relationMask & self::RELATION_SEQUENCE) {
            $count += $this -> relationCount(self::RELATION_SEQUENCE);
        }

        if ($relationMask & self::RELATION_VIEW) {
            $count += $this -> relationCount(self::RELATION_VIEW);
        }

        return $count;
    }

    public function relationList($relationType = eZDBInterface::RELATION_TABLE) {
        switch ($relationType) {
            case self::RELATION_TABLE:
                return $this -> DBConnection -> getSchemaManager() -> listTables();
            case self::RELATION_SEQUENCE:
                return $this -> DBConnection -> getSchemaManager() -> listSequences();
            case self::RELATION_VIEW:
                return $this -> DBConnection -> getSchemaManager() -> listViews();
        }

        return [];
    }

    public function relationMatchRegexp($relationType) {
        /** @todo STUB */
        eZDebug::writeError(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), 'eZDBInterface method not implemented');
    }

    public function removeDatabase($dbName) {
        $this -> DBConnection -> getSchemaManager() -> dropDatabase($dbName);
    }

    public function removeRelation($relationName, $relationType) {
        switch ($relationType) {
            case self::RELATION_TABLE:
                $this -> DBConnection -> getSchemaManager() -> dropTable($relationName);
                break;
            case self::RELATION_SEQUENCE:
                $this -> DBConnection -> getSchemaManager() -> dropSequence($relationName);
                break;
            case self::RELATION_VIEW:
                $this -> DBConnection -> getSchemaManager() -> dropView($relationName);
                break;
            case self::RELATION_INDEX:
                $this -> DBConnection -> getSchemaManager() -> dropIndex($relationName, null);
                break;
        }
    }

    public function rollbackQuery() {
        try {
            $this -> DBConnection -> rollBack();

            return true;
        } catch (ConnectionException $e) {
            eZDebug::writeError($e -> getMessage(), __METHOD__);

            return false;
        }
    }

    public function setError($connection = false) {
        $this -> ErrorMessage = $this -> DBConnection -> errorInfo();
        $this -> ErrorNumber = $this -> DBConnection -> errorCode();
    }

    public function subString($string, $from, $len = null) {
        return $this -> DBConnection -> getDatabasePlatform()
            -> getSubstringExpression($string, $from, $len);
    }

    /**
     * Only used in \eZDBTools::isEmpty()
     * which is, in turn, never used.
     * Pretty useless.
     */
    public function supportedRelationTypeMask() {
        return
            self::RELATION_TABLE_BIT |
            self::RELATION_SEQUENCE_BIT |
            self::RELATION_VIEW_BIT |
            self::RELATION_INDEX_BIT|
            self::RELATION_NONE;
    }

    public function supportedRelationTypes() {
        return [
            self::RELATION_TABLE,
            self::RELATION_SEQUENCE,
            self::RELATION_VIEW,
            self::RELATION_INDEX,
        ];
    }

    public function supportsDefaultValuesInsertion() {
        /**
         * returns false inconditionnaly because of the use cases,
         * should override eZOrder & eZUrlAliasML ::getNewID()
         */
        return false;
    }

    public function unlock() {
        /** @todo STUB */
        eZDebug::writeError(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), 'eZDBInterface method not implemented');
    }
}
